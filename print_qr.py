import socket


def print_qr(lines: str):
    host, port = "BRWF0A654ED0C47", 9100

    text = format_text(lines)

    # Create a socket (SOCK_STREAM means a TCP socket)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Connect to server and send data
        sock.connect((host, port))
        sock.sendall(bytes(text + "\n", "utf-8"))

        # Receive data from the server and shut down
        received = str(sock.recv(1024), "utf-8")

        print(received)


def format_text(line: str):
    esc = "\u001B"
    esc_p = f"{esc}ia\u0000"
    init = f"{esc}@"
    linefeed = f"{esc}2"
    qr = f"{esc}iQ\u000A0000040"
    qr_end = '\\\\\\'
    font = f"{esc}k\u000B"
    fontHeight = f"{esc}X\u0000\u0043\u0000"
    advance = "\u000C"
    text = line
    return f"{esc_p}{init}{linefeed}{font}{fontHeight}{qr}{text}{qr_end}{advance}"
