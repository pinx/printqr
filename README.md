# Print QR codes with Brother label printer

## Usage

Prepare a text file. Each line will be printed as a separate QR code.

```shell
print_qr my_codes.txt
```

## Development

The printer address and port number are hard coded. Communication with the printer is over TCP/IP.