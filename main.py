import sys

from print_qr import print_qr

if __name__ == '__main__':
    if sys.argv.count() < 1:
        print("Specify input file name on the command line")
        exit(1)
    filename = sys.argv[0]
    file = open(filename, 'r')
    lines = file.readlines()
    for line in lines:
        print_qr(line)
